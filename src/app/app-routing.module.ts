import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from "@angular/router";
import {CategoriasComponent} from "./componentes/categorias/categorias.component";
import {SubCategoriasComponent} from "./componentes/sub-categorias/sub-categorias.component";
import {FichaClinicaComponent} from "./componentes/ficha-clinica/ficha-clinica.component";
import {HorarioExcepcion} from "./modelos/horarioExcepcion";
import {AdministracionComponent} from "./componentes/administracion/administracion.component";
import {ServicioComponent} from "./componentes/servicio/servicio.component";
import {HorarioAtencionComponent} from "./componentes/horario-atencion/horario-atencion.component";
import {HorarioExcepcionComponent} from "./componentes/horario-excepcion/horario-excepcion.component";
import {ReservaComponent} from "./componentes/reserva/reserva.component";
import {ReporteComponent} from "./componentes/reporte/reporte.component";

const routes: Routes = [{
  path: 'categoria',
  component: CategoriasComponent
  },
  {
    path: 'tipoProducto',
    component: SubCategoriasComponent

  },
  {
    path: 'fichaClinica',
    component: FichaClinicaComponent

  },
  {
    path: 'tipoProducto',
    component: SubCategoriasComponent

  },
  {
    path: 'administracion',
    component: AdministracionComponent

  },
  {
    path: 'servicio',
    component: ServicioComponent

  },
  {
    path: 'horarioAtencion',
    component: HorarioAtencionComponent

  },
  {
    path: 'horarioExcepcion',
    component: HorarioExcepcionComponent

  },
  {
    path: 'reserva',
    component: ReservaComponent

  },
  {
    path: 'reporte',
    component: ReporteComponent

  }]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {
}




