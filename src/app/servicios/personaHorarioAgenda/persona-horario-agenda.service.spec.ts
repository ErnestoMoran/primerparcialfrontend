import { TestBed } from '@angular/core/testing';

import { PersonaHorarioAgendaService } from './persona-horario-agenda.service';

describe('PersonaHorarioAgendaService', () => {
  let service: PersonaHorarioAgendaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PersonaHorarioAgendaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
