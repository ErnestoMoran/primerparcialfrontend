import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PresentacionProducto} from "../../modelos/presentacionProducto";
import {Persona} from "../../modelos/persona";

@Injectable({
  providedIn: 'root'
})
export class PersonaService {

  private URL_BASE = environment.API_URL;

  constructor(private http: HttpClient) {
  }

  getListaCompletaPersonas(): Observable<Persona[]>{
    return this.http.get<Persona[]>(`${this.URL_BASE}/persona`)
  }

  getListaFisioterapeutas(persona: any): Observable<PresentacionProducto[]>{
    return this.http.get<PresentacionProducto[]>(`${this.URL_BASE}/persona?ejemplo=`+encodeURI(JSON.stringify(persona)))
  }


  crearPersona(persona:any):Observable<Persona> {
    return this.http.post<Persona>(`${this.URL_BASE}/persona`,persona);
  }

  eliminarPersona(id: number){
    return this.http.delete(`${this.URL_BASE}/persona/${id}`);
  }

  modificarPersona(persona: any) {
    return this.http.put(`${this.URL_BASE}/persona`, persona)
  }


}
