import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {FichaClinica} from "../../modelos/fichaClinica";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class FichaClinicaService {
  private URL_BASE = environment.API_URL;

  constructor(private http: HttpClient) { }

  crearFicha(descripcion: string){
    return this.http.post<FichaClinica>(`${this.URL_BASE}/fichaClinica`,{descripcion})
  }

  modificarFicha(descripcion: string){
    return this.http.put<FichaClinica>(`${this.URL_BASE}/fichaClinica`,{descripcion})
  }

  getFichasFechas(fechaFicha:any): Observable<FichaClinica[]>{
    return this.http.get<FichaClinica[]>(`${this.URL_BASE}/fichaClinica?ejemplo=`+encodeURI(JSON.stringify(fechaFicha)))
  }

  getFichasIdPacientes(idPaciente:any): Observable<FichaClinica[]>{
    return this.http.get<FichaClinica[]>(`${this.URL_BASE}/fichaClinica?ejemplo=`+encodeURI(JSON.stringify(idPaciente)))
  }

  getFichasIdFisio(idFisio:any): Observable<FichaClinica[]>{
    return this.http.get<FichaClinica[]>(`${this.URL_BASE}/fichaClinica?ejemplo=`+encodeURI(JSON.stringify(idFisio)))
  }

  getFichasTipoProducto(idProducto:any): Observable<FichaClinica[]>{
    return this.http.get<FichaClinica[]>(`${this.URL_BASE}/fichaClinica?ejemplo=`+encodeURI(JSON.stringify(idProducto)))
  }

}

