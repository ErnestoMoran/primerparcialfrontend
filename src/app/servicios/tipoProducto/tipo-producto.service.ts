import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Categoria} from "../../modelos/categoria";
import {TipoProducto} from "../../modelos/tipoProducto";

@Injectable({
  providedIn: 'root'
})
export class TipoProductoService {

  private URL_BASE = environment.API_URL;

  constructor(private http: HttpClient) {
  }

  getListaTipoProducto(){
    return this.http.get<TipoProducto[]>(`${this.URL_BASE}/tipoProducto`)
  }

  crearTipoProducto(categoria: Categoria, descripcion: string) {
    return this.http.post<TipoProducto>(`${this.URL_BASE}/tipoProducto`, {idCategoria:categoria,descripcion:descripcion});
  }

  eliminarTipoProducto(tipoProducto: TipoProducto){
    return this.http.delete(`${this.URL_BASE}/tipoProducto/${tipoProducto.idTipoProducto}`);
  }

  modificarTipoProducto(tipoProducto: TipoProducto){
    return this.http.put(`${this.URL_BASE}/tipoProducto`,{idTipoProducto:tipoProducto.idTipoProducto,descripcion:tipoProducto.descripcion,idCategoria:tipoProducto.idCategoria});
  }
}
