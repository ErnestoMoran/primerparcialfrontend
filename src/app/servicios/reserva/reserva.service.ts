import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ListaReserva, Reserva} from "../../modelos/reserva";

@Injectable({
  providedIn: 'root'
})
export class ReservaService {

  private URL_BASE=environment.API_URL

  constructor(private http: HttpClient) { }

  getAgendaFisioIdFecha(idFisio:number,fecha:string):Observable<Reserva[]>{
    return this.http.get<Reserva[]>(`${this.URL_BASE}/persona/`+idFisio+'/agenda?fecha='+fecha)
  }

  getAgendaLibresFisioIdFecha(idFisio:number,fecha:string):Observable<Reserva[]>{
    return this.http.get<Reserva[]>(`${this.URL_BASE}/persona/`+idFisio+'/agenda?fecha='+fecha+'&disponible=S')
  }

  crearReserva(reserva : Reserva){
    var headers= new HttpHeaders();
    headers.set('usuario','usuario2')
    return this.http.post<Reserva>(`${this.URL_BASE}/reserva`,{reserva},{headers});
  }

  getReservaRealizadasFisioIdFecha(reserva:any):Observable<ListaReserva>{
    return this.http.get<ListaReserva>(`${this.URL_BASE}/reserva?ejemplo=`+encodeURI(JSON.stringify(reserva)))
  }

  getReservaRealizadasFisioIdEntreFechas(reserva:any):Observable<ListaReserva>{
    return this.http.get<ListaReserva>(`${this.URL_BASE}/reserva?ejemplo=`+encodeURI(JSON.stringify(reserva)))
  }

  getReservaRealizadasFisioId(reserva:any):Observable<Reserva[]>{
    return this.http.get<Reserva[]>(`${this.URL_BASE}/reserva?ejemplo=`+encodeURI(JSON.stringify(reserva)))
  }

  eliminarReserva(reservaId:number){
    return this.http.delete(`${this.URL_BASE}/reserva/`+reservaId)
  }

  modificarReserva(reserva: Reserva){
    return this.http.put(`${this.URL_BASE}/persona/`,{reserva})
  }

  getTodasReservas():Observable<Reserva[]>{
    return this.http.get<Reserva[]>(`${this.URL_BASE}/reserva`)
  }
}

