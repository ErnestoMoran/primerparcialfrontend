import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Categoria} from "../../modelos/categoria";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {
  private URL_BASE = environment.API_URL;

  constructor(private http: HttpClient) {
  }

  getListaCategoria(): Observable<Categoria[]> {
    return this.http.get<Categoria[]>(`${this.URL_BASE}/categoria`)
  }

  crearCategoria(descripcion: string) {
    console.log(descripcion);
    return this.http.post<Categoria>(`${this.URL_BASE}/categoria`, {descripcion});
  }

  eliminarCategoria(categoria: number){
    return this.http.delete(`${this.URL_BASE}/categoria/${categoria}`);
  }

  modificarCategoria(categoria: number, descripcion: string){
    return this.http.put(`${this.URL_BASE}/categoria`,{categoria,descripcion});
  }
}
