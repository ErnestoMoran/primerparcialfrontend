import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Servicio} from "../../modelos/servicio";
import {Categoria} from "../../modelos/categoria";
import {TipoProducto} from "../../modelos/tipoProducto";
import {PresentacionProducto} from "../../modelos/presentacionProducto";
import {stringify} from "@angular/compiler/src/util";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class PresentacionProductoService {


  private URL_BASE = environment.API_URL;

  constructor(private http: HttpClient) {
  }

  getListaPresentacionProductoFiltrado(pres: any): Observable<PresentacionProducto[]>{
    return this.http.get<PresentacionProducto[]>(`${this.URL_BASE}/presentacionProducto?ejemplo=`+encodeURI(JSON.stringify(pres)))
  }

  getListaPresentacionProductoQueContiene(pres: any): Observable<PresentacionProducto[]>{
    return this.http.get<PresentacionProducto[]>(`${this.URL_BASE}/presentacionProducto?ejemplo=`+encodeURI(JSON.stringify(pres)+"&like=S"))
  }

    crearPresentacionProducto(pres:any):Observable<PresentacionProducto> {
      return this.http.post<PresentacionProducto>(`${this.URL_BASE}/presentacionProducto`,pres);
    }

    eliminarPresentacionProducto(id: number){
      return this.http.delete(`${this.URL_BASE}/presentacionProducto/${id}`);
    }

    modificarPresentacionProducto(tipoProducto: any) {
      return this.http.put(`${this.URL_BASE}/presentacionProducto`, tipoProducto)
    }

}
