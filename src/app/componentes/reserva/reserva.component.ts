import { Component, OnInit } from '@angular/core';
import {ListaReserva, Reserva} from "../../modelos/reserva";
import {ReservaService} from "../../servicios/reserva/reserva.service";
import {Persona} from "../../modelos/persona";

@Component({
  selector: 'app-reserva',
  templateUrl: './reserva.component.html',
  styleUrls: ['./reserva.component.css']
})
export class ReservaComponent implements OnInit {

  public listaReserva: Reserva[]=[];
  public reservas:ListaReserva=new ListaReserva();
  public mensajeError: string = "";


  constructor(private service: ReservaService) { }

  ngOnInit(): void {
   /* this.service.getTodasReservas().subscribe((lista:any)=>{
      this.listaReserva=lista.lista;
      console.log(this.listaReserva);
    },(error:ErrorEvent) =>{
       this.mensajeError=error.error.message;
       console.log(this.mensajeError);
    })
    this.service.getAgendaFisioIdFecha(4,"20190903").subscribe((lista:any)=>{
      this.listaReserva=lista;
      console.log(this.listaReserva);
    },(error:ErrorEvent) =>{
      this.mensajeError=error.error.message;
      console.log(this.mensajeError);
    })
    this.service.getReservaRealizadasFisioIdFecha(reserva).subscribe((lista:any)=>{
      this.listaReserva=lista;
      console.log(this.listaReserva);
    },(error:ErrorEvent) =>{
      this.mensajeError=error.error.message;
      console.log(this.mensajeError);
    })
    this.service.getReservaRealizadasFisioIdEntreFechas(reserva).subscribe((lista:any)=>{
      this.reservas=lista.lista;
      console.log(this.reservas);
    },(error:ErrorEvent) =>{
      this.mensajeError=error.error.message;
      console.log(this.mensajeError);
    })*/
    var persona={
      idPersona:1
    }
    var reserva={
      idCliente:persona
      //fechaDesdeCadena:'20190903',
      //fechaHastaCadena:'20190920'

    }
    this.service.getReservaRealizadasFisioId(reserva).subscribe((lista:any)=>{
      this.reservas=lista;
      console.log(this.reservas);
    },(error:ErrorEvent) =>{
      this.mensajeError=error.error.message;
      console.log(this.mensajeError);
    })
  }

}
