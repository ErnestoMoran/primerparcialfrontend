import {Component, Inject, OnInit} from '@angular/core';
import {FormControl} from "@angular/forms";
import {MatDialogRef} from "@angular/material/dialog";
import {MAT_DIALOG_DATA, MatDialog} from "@angular/material/dialog";
import {MatTableDataSource} from "@angular/material/table";
import {ReservaService} from "../../servicios/reserva/reserva.service";
import {FichaClinicaService} from "../../servicios/fichaClinica/ficha-clinica.service";
import {Reserva} from "../../modelos/reserva";
import {FichaClinica} from "../../modelos/fichaClinica";

@Component({
  selector: 'app-ficha-clinica',
  templateUrl: './ficha-clinica.component.html',
  styleUrls: ['./ficha-clinica.component.css']
})
export class FichaClinicaComponent implements OnInit {

  fechaDesde = new FormControl(new Date());
  fechaHasta = new FormControl(new Date());
  serializedDate = new FormControl((new Date()).toISOString());
  public listaFichas: FichaClinica[]=[];
  public mensajeError: string = "";




  constructor(
    public dialog: MatDialog,
    private service: FichaClinicaService
  ) { }

  ngOnInit(): void {

    var persona={
      idPersona:2
    }
    var reserva={
      idEmpleado:persona
      //fechaDesdeCadena:'20190901',
      //fechaHastaCadena:'20190901'
    }
    var idTipoProducto={
      idTipoProducto:111
    }
    var producto={
      idTipoProducto:idTipoProducto
      //fechaDesdeCadena:'20190901',
      //fechaHastaCadena:'20190901'
    }
/*
    this.service.getFichasFechas(reserva).subscribe((lista:any)=>{
      this.listaFichas=lista;
      console.log(this.listaFichas);
    },(error:ErrorEvent) =>{
      this.mensajeError=error.error.message;
      console.log(this.mensajeError);
    })
    this.service.getFichasIdPacientes(reserva).subscribe((lista:any)=>{
      this.listaFichas=lista;
      console.log(this.listaFichas);
    },(error:ErrorEvent) =>{
      this.mensajeError=error.error.message;
      console.log(this.mensajeError);
    })
    this.service.getFichasIdFisio(reserva).subscribe((lista:any)=>{
      this.listaFichas=lista;
      console.log(this.listaFichas);
    },(error:ErrorEvent) =>{
      this.mensajeError=error.error.message;
      console.log(this.mensajeError);
    })
 */

    this.service.getFichasTipoProducto(producto).subscribe((lista:any)=>{
      this.listaFichas=lista;
      console.log(this.listaFichas);
    },(error:ErrorEvent) =>{
      this.mensajeError=error.error.message;
      console.log(this.mensajeError);
    })

  }

  openDialog(): void {
   // this.mensajeExito = '';

    const dialogRef = this.dialog.open(DialogFichaClinica, {
      width: '800px'
//      data: {nombre: this.nombre_paciente, apellido: this.apellido_paciente}
    });
/*
    dialogRef.afterClosed().subscribe(result => {
      this.paciente = result
      this.nombre_paciente.setValue(this.paciente.nombre_paciente);
      this.apellido_paciente.setValue(this.paciente.apellido_paciente);
      this.cedula_paciente.setValue(this.paciente.cedula_paciente.toString());
    });
    */


  }


}



@Component({
  selector: 'dialog-ficha-clinica',
  templateUrl: 'dialog-ficha-clinica-empleado.html',
  styleUrls: ['./ficha-clinica.component.css']
})
export class DialogFichaClinica {
/*
  public mensajeExito: string = '';
  public mensajeError: string = '';

  public columns = ['nombre','apellido','cedula']

  // @ts-ignore
  @ViewChild(MatTable) table: MatTable<Paciente>;
  public personas: Paciente[] = [];


  // @ts-ignore
  public dataSource = this.personas;
*/
  constructor(
    public dialogRef: MatDialogRef<DialogFichaClinica>,
   // @Inject(MAT_DIALOG_DATA) public data: Paciente,
   // private servicio: SistemaMedicoService
   ) {

  }


  onNoClick(): void {
    this.dialogRef.close();
  }
/*
  buscarPaciente(nombre: string, apellido: string){
    this.mensajeError = '';
    this.mensajeExito = '';
    this.servicio.getBuscarPacientesPorNombreApellido(nombre, apellido)
      .subscribe((pacientesEncontrados: Paciente[]) => {
          this.personas = pacientesEncontrados;
          this.dataSource = this.personas;
          this.table.renderRows();
        },
        (error: ErrorEvent) => {
          this.mensajeError = error.error.message;
        });
  }

  seleccionarPaciente(paciente:Paciente){
    this.data = paciente;
  }
*/
}





