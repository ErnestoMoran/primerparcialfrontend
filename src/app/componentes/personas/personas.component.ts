import {Component, OnInit, ViewChild} from '@angular/core';
import {PresentacionProducto, Producto} from "../../modelos/presentacionProducto";
import {TipoProducto} from "../../modelos/tipoProducto";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {PresentacionProductoService} from "../../servicios/presentacionProducto/presentacion-producto.service";
import {Persona} from "../../modelos/persona";
import {PersonaService} from "../../servicios/persona/persona.service";

@Component({
  selector: 'app-pacientes',
  templateUrl: './personas.component.html',
  styleUrls: ['./personas.component.css']
})
export class PersonasComponent implements OnInit {

  public listaPersonas: Persona[] = [];
  public listaFisioterapeutas: Persona[] = [];
  public mensajeError: string = "";
  public mensajeExito: string = "";


  public columns = ['idCategoria','descripcion','actions']
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild(MatTable) table: MatTable<Persona>;


  dataSourceFisioterapeutas= new MatTableDataSource<Persona>(this.listaFisioterapeutas);
  dataSourcePersonas= new MatTableDataSource<Persona>(this.listaPersonas);

  constructor(private service: PersonaService) { }

  ngOnInit(): void {
    /***** GET *****/
      //Obtiene las presentaciones cuyo tipo de producto sea de id 2
    this.service.getListaCompletaPersonas()
      .subscribe((lista:any)=>{
        console.log(lista);
        this.listaPersonas = lista.lista;
        this.dataSourcePersonas = new MatTableDataSource<Persona>(this.listaPersonas);
        this.dataSourcePersonas.paginator = this.paginator;
        this.dataSourcePersonas.sort = this.sort;
      },(error: ErrorEvent) => {
        this.mensajeError = error.error.message;
      })


    //Obtiene solo los pacientes, es decir personas que no tienen usuario
    var pac = {
      soloUsuariosDelSistema:true
    }
    this.service.getListaFisioterapeutas(pac)
      .subscribe((lista:any)=>{
        console.log(lista);
        this.listaFisioterapeutas = lista.lista;
        this.dataSourceFisioterapeutas = new MatTableDataSource<Persona>(this.listaFisioterapeutas);
        this.dataSourceFisioterapeutas.paginator = this.paginator;
        this.dataSourceFisioterapeutas.sort = this.sort;
      },(error: ErrorEvent) => {
        this.mensajeError = error.error.message;
      })


    /***** POST *****/
/*
        var creacion = {
      nombre: "Fulanito",
      apellido: "Mengano",
      email: "mengano@gmail.com",
      telefono: "0931-000-000",
      ruc: "456789",
      cedula: "654123",
      tipoPersona: "FISICA",
      fechaNacimiento: "1993-08-25 00:00:00"
    }
        this.service.crearPersona(creacion)
          .subscribe((persona)=>{
              //this.recargarLista();
            console.log(persona);
            },
            (error:ErrorEvent) => {
              this.mensajeError = error.error.message;
            })

*/

    /***** PUT *****/
    // No se puede hacer, dice "registro ya existe"
    /*var modificacion = {
        idPersona: 188,
        nombre: "Fulanito2",
        apellido: "Mengano2",
        email: "mengano@gmail.com",
        telefono: "0931-000-000",
        ruc: "456789",
        cedula: "654123",
        tipoPersona: "FISICA",
        fechaNacimiento: "1993-08-25 00:00:00"
      }
     this.service.modificarPersona(modificacion)
       .subscribe((value)=>{
         console.log(value)
         },
         (error:ErrorEvent) => {
           this.mensajeError = error.error.message;
         })
*/

    /***** DELETE *****/
 /*      var eliminarPres = {
          idPresentacionProducto: 188
        }
        this.service.eliminarPersona(eliminarPres.idPresentacionProducto)
          .subscribe((value)=>{
              //this.recargarLista();
          console.log(value)
            },
            (error:ErrorEvent) => {
              this.mensajeError = error.error.message;
            })
*/
  }



}
