import {Component, OnInit, ViewChild} from '@angular/core';
import {TipoProductoService} from "../../servicios/tipoProducto/tipo-producto.service";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {Categoria} from "../../modelos/categoria";
import {TipoProducto} from "../../modelos/tipoProducto";
import {FormControl, Validators} from "@angular/forms";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {PresentacionProducto} from "../../modelos/presentacionProducto";
import {CategoriaService} from "../../servicios/categoria/categoria.service";

@Component({
  selector: 'app-sub-categorias',
  templateUrl: './sub-categorias.component.html',
  styleUrls: ['./sub-categorias.component.css']
})
export class SubCategoriasComponent implements OnInit {

  public listaTipoProducto: TipoProducto[] = [];
  public mensajeError: string = "";
  public mensajeExito: string = "";
  public nombreCategoria=new FormControl('',Validators.required);
  public descripcion=new FormControl('',Validators.required);
  public categoriaAuxiliar = new Categoria();
  public descripcionTipoProducto: string = "";
  public tipoProductoAuxiliar = new TipoProducto();


  public columns = ['idCategoria','descripcion','actions']
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild(MatTable) table: MatTable<TipoProducto>;


  dataSource= new MatTableDataSource<TipoProducto>(this.listaTipoProducto);

  constructor(private service: TipoProductoService,
              private servicioCategoria: CategoriaService) { }

  ngOnInit(): void {
    /***** GET *****/
    this.service.getListaTipoProducto()
      .subscribe((lista:any)=>{
        console.log(lista);
        this.listaTipoProducto = lista.lista;
        this.dataSource = new MatTableDataSource<TipoProducto>(this.listaTipoProducto);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },(error: ErrorEvent) => {
        this.mensajeError = error.error.message;
      })


    /***** POST *****/

    /*this.categoriaAuxiliar = {idCategoria:2,descripcion:""};
    this.descripcionTipoProducto = "prueba de tipo producto x"
    this.service.crearTipoProducto(this.categoriaAuxiliar,this.descripcionTipoProducto)
      .subscribe((tipoProducto)=>{
          //this.recargarLista();
        console.log(tipoProducto);
        },
        (error:ErrorEvent) => {
          this.mensajeError = error.error.message;
        })
      */


    /***** PUT *****/
/*    this.categoriaAuxiliar = {idCategoria:2,descripcion:""};
    this.descripcionTipoProducto = "prueba de tipo producto m";
    this.tipoProductoAuxiliar = {idTipoProducto:131,descripcion:this.descripcionTipoProducto,idCategoria:this.categoriaAuxiliar}

    this.service.modificarTipoProducto(this.tipoProductoAuxiliar)
      .subscribe((value)=>{
        },
        (error:ErrorEvent) => {
          this.mensajeError = error.error.message;
        })
    this.categoriaAuxiliar = new Categoria();
*/


    /***** DELETE *****/

    /*this.categoriaAuxiliar = {idCategoria:2,descripcion:""};
    this.descripcionTipoProducto = "prueba de tipo producto m";
    this.tipoProductoAuxiliar = {idTipoProducto:138,descripcion:this.descripcionTipoProducto,idCategoria:this.categoriaAuxiliar}

    this.service.eliminarTipoProducto(this.tipoProductoAuxiliar)
      .subscribe((value)=>{
          //this.recargarLista();

        },
        (error:ErrorEvent) => {
          this.mensajeError = error.error.message;
        })

    */

  }

}
