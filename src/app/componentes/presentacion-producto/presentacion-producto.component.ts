import {Component, OnInit, ViewChild} from '@angular/core';
import {TipoProducto} from "../../modelos/tipoProducto";
import {FormControl, Validators} from "@angular/forms";
import {Categoria} from "../../modelos/categoria";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {TipoProductoService} from "../../servicios/tipoProducto/tipo-producto.service";
import {PresentacionProductoService} from "../../servicios/presentacionProducto/presentacion-producto.service";
import {PresentacionProducto, Producto} from "../../modelos/presentacionProducto";

@Component({
  selector: 'app-presentacion-producto',
  templateUrl: './presentacion-producto.component.html',
  styleUrls: ['./presentacion-producto.component.css']
})
export class PresentacionProductoComponent implements OnInit {

  public listaPresentacionProducto: PresentacionProducto[] = [];
  public mensajeError: string = "";
  public mensajeExito: string = "";
  public presentacionProducto = new PresentacionProducto();
  public idTipoProducto = new TipoProducto();
  public producto = new Producto();


  public columns = ['idCategoria','descripcion','actions']
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild(MatTable) table: MatTable<PresentacionProducto>;


  dataSource= new MatTableDataSource<PresentacionProducto>(this.listaPresentacionProducto);

  constructor(private service: PresentacionProductoService) { }

  ngOnInit(): void {
    /***** GET *****/
    //Obtiene las presentaciones cuyo tipo de producto sea de id 2
   var presProd = {
      idProducto: {
        idTipoProducto:{
          idTipoProducto: 38
        }
      }
    }
    this.service.getListaPresentacionProductoFiltrado(presProd)
      .subscribe((lista:any)=>{
        console.log(lista);
        this.listaPresentacionProducto = lista.lista;
        this.dataSource = new MatTableDataSource<PresentacionProducto>(this.listaPresentacionProducto);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },(error: ErrorEvent) => {
        this.mensajeError = error.error.message;
      })


    //Obtiene las presentaciones cuyo nombre contenga el carácter “1”
    var presProdContiene = {
      nombre:"1"
    }

    this.service.getListaPresentacionProductoQueContiene(presProdContiene)
      .subscribe((lista:any)=>{
        console.log(lista);
        this.listaPresentacionProducto = lista.lista;
        this.dataSource = new MatTableDataSource<PresentacionProducto>(this.listaPresentacionProducto);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },(error: ErrorEvent) => {
        this.mensajeError = error.error.message;
      })


    /***** POST *****/
/*
    var nuevaPresentacion = {
      codigo: 190,
      flagServicio: "S",
      idProducto: {
        idProducto: 38
      },
      nombre: "Prueba de Presentacion1",
      existenciaProducto: {
        precioVenta: 200000
      }
    }
    this.service.crearPresentacionProducto(nuevaPresentacion)
      .subscribe((tipoProducto)=>{
          //this.recargarLista();
        console.log(tipoProducto);
        },
        (error:ErrorEvent) => {
          this.mensajeError = error.error.message;
        })

*/

    /***** PUT *****/
      // No se puede hacer, dice "registro ya existe"
   /* var modiPresent = {
      idPresentacionProducto: 350,
      nombre: "Prueba de Modificacion Presentacion1",
      existenciaProducto: {
        precioVenta: 300000
      }

    }
    this.service.modificarPresentacionProducto(modiPresent)
      .subscribe((value)=>{
        console.log(value)
        },
        (error:ErrorEvent) => {
          this.mensajeError = error.error.message;
        })
*/

    /***** DELETE *****/
/*   var eliminarPres = {
      idPresentacionProducto: 354
    }
    this.service.eliminarPresentacionProducto(eliminarPres.idPresentacionProducto)
      .subscribe((value)=>{
          //this.recargarLista();

        },
        (error:ErrorEvent) => {
          this.mensajeError = error.error.message;
        })
*/
  }


}
