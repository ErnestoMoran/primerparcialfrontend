import {AfterViewInit, Component, Inject, OnInit, ViewChild} from '@angular/core';
import {CategoriaService} from "../../servicios/categoria/categoria.service";
import {Categoria} from "../../modelos/categoria";
import {MatTable, MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {FormControl, Validators} from "@angular/forms";
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  public listaCategorias: Categoria[] = [];
  public mensajeError: string = "";
  public mensajeExito: string = "";
  public nombreCategoria=new FormControl('',Validators.required);
  public descripcion=new FormControl('',Validators.required);
  public categoriaAuxiliar = new Categoria();


  public columns = ['idCategoria','descripcion','actions']
  // @ts-ignore
  @ViewChild(MatPaginator) paginator: MatPaginator;
  // @ts-ignore
  @ViewChild(MatSort) sort: MatSort;
  // @ts-ignore
  @ViewChild(MatTable) table: MatTable<Categoria>;


  dataSource= new MatTableDataSource<Categoria>(this.listaCategorias);


  constructor(private service:CategoriaService,
              public dialog: MatDialog) {
  }

  ngOnInit(): void {
    this.service.getListaCategoria()
      .subscribe((lista:any)=>{
        this.listaCategorias = lista.lista;
        this.dataSource = new MatTableDataSource<Categoria>(this.listaCategorias);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;


      },(error: ErrorEvent) => {
        this.mensajeError = error.error.message;
      })
  }

  crearCategoria(descripcion:string){
    this.service.crearCategoria(descripcion)
      .subscribe(()=>{
        this.recargarLista();
      },
        (error:ErrorEvent) => {
          this.mensajeError = error.error.message;
        })
    this.mensajeExito = 'La categoría ${descripcion} fue creada exitosamente'
  }

  recargarLista(){
    this.service.getListaCategoria()
      .subscribe((lista:any)=>{
        this.listaCategorias = lista.lista;
        this.dataSource.data = this.listaCategorias;
      },(error: ErrorEvent) => {
        this.mensajeError = error.error.message;
      })

  }

  filtrar(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  agregar(){

  }


  editar(categoria: Categoria){
    this.categoriaAuxiliar.idCategoria = categoria.idCategoria;
    this.categoriaAuxiliar.descripcion = categoria.descripcion;
    this.openDialog();
  }

  eliminar(categoria: Categoria){
    //console.log("Selecciono la categoria" + categoria.idCategoria + " con la descripcion " + categoria.descripcion)
    this.service.eliminarCategoria(categoria.idCategoria)
      .subscribe((value)=>{
          this.recargarLista();

        },
        (error:ErrorEvent) => {
          this.mensajeError = error.error.message;
        })
    this.mensajeExito = 'La categoría ${descripcion} fue creada exitosamente'

    this.mensajeExito = "La categoria ha sido eliminada exitosamente"

  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogoModificarCategoria, {
      width: '250px',
      data: {id: this.categoriaAuxiliar.idCategoria, descripcion: this.categoriaAuxiliar.descripcion}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined){
        this.service.modificarCategoria(this.categoriaAuxiliar.idCategoria, result)
          .subscribe((value)=>{
            },
            (error:ErrorEvent) => {
              this.mensajeError = error.error.message;
            })
        this.categoriaAuxiliar = new Categoria();
      }
      });

  }

}




@Component({
  selector: 'dialog-modificar-categoria',
  templateUrl: './dialogos/modificarCategoria.html',
})
export class DialogoModificarCategoria {

  constructor(
    public dialogRef: MatDialogRef<DialogoModificarCategoria>,
    @Inject(MAT_DIALOG_DATA) public data: Categoria) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

}

