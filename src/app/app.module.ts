import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {MatTableModule} from "@angular/material/table";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatNativeDateModule} from "@angular/material/core";
import {MatInputModule} from "@angular/material/input";
import {MatButtonModule} from "@angular/material/button";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatSortModule} from "@angular/material/sort";
import {MatSelectModule} from "@angular/material/select";
import { AppRoutingModule } from './app-routing.module';
import {ServicioComponent} from "./componentes/servicio/servicio.component";
import {ReporteComponent} from "./componentes/reporte/reporte.component";
import {DialogFichaClinica, FichaClinicaComponent} from './componentes/ficha-clinica/ficha-clinica.component';
import { ReservaComponent } from './componentes/reserva/reserva.component';
import {CategoriasComponent, DialogoModificarCategoria} from './componentes/categorias/categorias.component';
import { SubCategoriasComponent } from './componentes/sub-categorias/sub-categorias.component';
import { AdministracionComponent } from './componentes/administracion/administracion.component';
import { PersonasComponent } from './componentes/personas/personas.component';
import {MatIconModule} from "@angular/material/icon";
import {MatDialogModule} from "@angular/material/dialog";
import { HorarioAtencionComponent } from './componentes/horario-atencion/horario-atencion.component';
import { HorarioExcepcionComponent } from './componentes/horario-excepcion/horario-excepcion.component';
import {MatPaginatorModule} from "@angular/material/paginator";
import { PresentacionProductoComponent } from './componentes/presentacion-producto/presentacion-producto.component';


@NgModule({
  declarations: [
    AppComponent,
    ServicioComponent,
    ReporteComponent,
    FichaClinicaComponent,
    ReservaComponent,
    CategoriasComponent,
    SubCategoriasComponent,
    AdministracionComponent,
    PersonasComponent,
    DialogFichaClinica,
    HorarioAtencionComponent,
    HorarioExcepcionComponent,
    DialogoModificarCategoria,
    PresentacionProductoComponent
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatProgressSpinnerModule,
        MatFormFieldModule,
        MatInputModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatToolbarModule,
        HttpClientModule,
        MatSortModule,
        FormsModule,
        MatInputModule,
        MatFormFieldModule,
        MatSelectModule,
        MatIconModule,
        ReactiveFormsModule,
        AppRoutingModule,
        MatDialogModule,
        MatPaginatorModule
    ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule { }
