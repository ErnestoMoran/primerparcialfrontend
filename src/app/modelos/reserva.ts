import {Persona} from "./persona";

export class Reserva{
  idReserva !: number;
  flagAsistio !: string;
  flagEstado !: string;
  observacion !: string;
  fechaCadena !: string;
  horaInicioCadena !: string;
  horaFinCadena !: string;
  idEmpleado !: Persona;
  idCliente !: Persona;
}
export class ListaReserva{
  lista!:Reserva[];
  totalDatos!:number
}
