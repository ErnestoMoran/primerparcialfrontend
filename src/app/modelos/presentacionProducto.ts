import {TipoProducto} from "./tipoProducto";

export class PresentacionProducto{
  idPresentacionProducto!: number;
  codigo !: string;
  flagServicio !: string;
  nombre !: string;
  descripcion !: string;
  idProducto !: Producto;
  existenciaProducto !: ExistenciaProducto;
}

export class Marca{
  idMarca !: number;
  descripcion !: string;
}

export class Producto{
  idProducto !: number;
  descripcion !: string;
  idTipoProducto !: TipoProducto;
  idMarca !: Marca
}

export class ExistenciaProducto{
  idExistenciaProducto !: number;
  cantidad !: number;
  precioVenta !: number;
}
