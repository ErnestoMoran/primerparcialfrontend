import {Persona} from "./persona";

export class HorarioExcepcion{
  fechaCadena !: string;
  horaAperturaCadena !: string;
  horaCierreCadena !: string;
  flagEsHabilitar !: string;
  idEmpleado !: Persona;
  intervaloMinutos !: number
}
