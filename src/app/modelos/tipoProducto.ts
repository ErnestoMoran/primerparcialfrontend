import {Categoria} from "./categoria";

export class TipoProducto{
  idTipoProducto!: number;
  descripcion!: string;
  idCategoria!: Categoria;
}
