import {Persona} from "./persona";

export class PersonaHorarioAgenda{
  dia !: number;
  horaAperturaCadena !: string;
  horaCierreCadena !: string;
  intervaloMinuto !: number;
  idEmpleado !: Persona;
}
