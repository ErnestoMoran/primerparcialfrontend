import {Persona} from "./persona";
import {TipoProducto} from "./tipoProducto";

export class FichaClinica{
  motivoConsulta !: string;
  diagnostico !: string;
  observacion !: string;
  idEmpleado !: Persona;
  idCliente !: Persona;
  idTipoProducto !: TipoProducto;
}
