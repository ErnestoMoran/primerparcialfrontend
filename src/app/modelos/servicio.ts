import {FichaClinica} from "./fichaClinica";
import {PresentacionProducto} from "./presentacionProducto";

export class Servicio{
  idServicio !: number;
  idFichaClinica !: FichaClinica;
  observacion !: string;
}

export class Detalle{
  cantidad !: number;
  idPresentacionProducto !: PresentacionProducto;
  idServicio !: Servicio;
}
